import { createRouter, createWebHistory } from 'vue-router'
import Loading from '@/views/loading.vue'
import PcLayout from '@/views/pc/layout/index.vue'
import MbLayout from '@/views/mb/layout/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'root',
      component: Loading
    },
    {
      path: '/pc',
      name: 'pc',
      component: PcLayout,
      redirect: '/pc/list/all',
      children: [
        {
          path: 'list/:mode',
          name: 'pc-list',
          component: () => import('@/views/pc/list/index.vue'),
        },
        {
          path: 'detail/:id',
          name: 'pc-detail',
          component: () => import('@/views/pc/detail/index.vue'),
        }
      ]
    },
    {
      path: '/mb',
      name: 'mb',
      component: MbLayout,
      redirect: '/mb/list/all',
      children: [
        {
          name: 'mb-list',
          path: 'list/:mode',
          component: () => import('@/views/mb/list/index.vue'),
        },
        {
          path: 'detail/:id',
          name: 'mb-detail',
          component: () => import('@/views/mb/detail/index.vue'),
        }
      ]
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('../views/RouterView.vue')
    }
  ]
})

export default router
