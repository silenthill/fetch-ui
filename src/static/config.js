const listFilters = [
  {
    key: 'type',
    name: '榜单类型',
    selected: 'week',
    items: [
      { key: 'week', tag: '周榜'},
      { key: 'month', tag: '月榜'},
      { key: 'quarter', tag: '季度榜'},
    ]
  },
  {
    key: 'list',
    name: '剧目类型',
    selected: 'all',
    items: [
      { key: 'all', tag: '全部'},
      { key: 'tv', tag: '电视剧'},
      { key: 'web_tv', tag: '网络剧'},
    ]
  },
  {
    key: 'platform',
    name: '平台',
    selected: 'all',
    items: [
      { key: 'all', tag: '全部'},
      { key: 'qq', tag: '腾讯视频'},
      { key: 'youku', tag: '优酷'},
      { key: 'iqiyi', tag: '爱奇艺'},
      { key: 'bili', tag: '哔哩哔哩'},
      { key: 'mgtv', tag: '芒果TV'},
    ]
  },
]
export { listFilters } 

