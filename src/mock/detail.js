export default {
  contents: {
    data: [
      { key: 'xxxx',content: 'xxxx'},
      { key: 'xxxx',content: 'xxxx'},
    ],
    columns: [
      {title: 'key', dataIndex: 'key', key: 'key', width: 200 },
      {title: 'content', dataIndex: 'content', key: 'content'}
    ],
  },
  characters: {
    data: [
      { char: '张楚岚', actor: '彭昱畅', tags: 'xxx, xxx, xxx, xxx' },
      { char: '冯宝宝', actor: '王影璐', tags: 'xxx, xxx, xxx, xxx' },
      { char: '王也', actor: '侯明昊', tags: 'xxx, xxx, xxx, xxx' },
      { char: '张灵玉', actor: '毕雯珺', tags: 'xxx, xxx, xxx, xxx' },
    ],
    columns: [
      {title: '人物', dataIndex: 'char', key: 'char', width: 200 },
      {title: '演员', dataIndex: 'actor', key: 'actor', width: 200 },
      {title: '人物性格', dataIndex: 'tags', key: 'tags' },
    ],
  },
  item: {
    index: 1, 
    pic: 'https://www.themoviedb.org/t/p/w300_and_h450_bestv2/96nRqqe6gAtmMjOMHZtilrgNdXM.jpg',
    bg: '	https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/1Lx5oXLVZclQ94jnfAwmanaey7P.jpg',
    name: '异人之下',
    score: '70%',
    plats: ['优酷'],
    director: "徐宏宇",
    writers: ["蒋峰", "陈仕澍(联合)","王子旋(联合)"],
    company: ["深圳市腾讯动漫有限公司","优酷信息技术（北京）有限公司"],
    onlineTime: "2023-08-04",
    description: "平凡少年张楚岚（彭昱畅 饰）因爷爷尸体的离奇失踪，被卷入前所未见的“异人”世界之中，面对“全性”突如其来的追杀、神秘少女冯宝宝（王影璐 饰）的突然闯入，张楚岚决心不再隐藏异能。随着对于爷爷前尘往事的追查，张楚岚渐渐融入了异人的江湖，而历史的谜团也逐渐浮现，在那背后似乎有个惊天的秘密，而冯宝宝的神秘身世似乎正是解开谜团的钥匙。面对着接踵而至的危机，少年异人们集结起来并建立了深厚的友谊和羁绊，在这条成长之路上，每个人都在寻找自己的道与义，一场光明与黑暗的对决蓄势待发……",
    tags: [ '剧情', '奇幻'],
    actors: [
      { character: '张楚岚', actor: '彭昱畅', pic: '', role: '男主' },
      { character: '冯宝宝', actor: '王影璐', pic: '', role: '女主' },
      { character: '王也', actor: '侯明昊', pic: '', role: '' },
      { character: '张灵玉', actor: '毕雯珺', pic: '', role: '' },
    ],
    // 添加更多属性
  },
}
