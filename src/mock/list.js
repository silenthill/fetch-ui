export default {
  topItems: [
    {
      index: 1, 
      pic: 'https://www.themoviedb.org/t/p/w300_and_h450_bestv2/96nRqqe6gAtmMjOMHZtilrgNdXM.jpg',
      bg: '	https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/1Lx5oXLVZclQ94jnfAwmanaey7P.jpg',
      name: '异人之下', score: '70%', plats: ['qq', 'iqiyi']
    },
    {
      index: 2,
      pic: 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/oTQK4cOCfpEeXAk20tZZ9eA3VYT.jpg',
      bg: 'https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/r8kNJqajV0Bvx9ZikFIssZgx2R5.jpg',
      name: '他从火光中走来', score: '60%', plats: ['qq', 'iqiyi']
    },
    {
      index: 3,
      pic: 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/udoNtgXxJHCYvb3pbFVhm3xrvPs.jpg',
      bg: '	https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/f5KVfnAjtzMv3IwFENLv5Vo14wM.jpg',
      name: '西出玉门', score: '50%', plats: ['qq', 'iqiyi']
    },
  ],
  otherItems: [
    { index: 4, name: '兰闺喜事', score: '40%', plats: ['qq', 'iqiyi'] },
    { index: 5, name: '云之羽', score: '30%', plats: ['qq', 'iqiyi'] },
    { index: 6, name: '南风知我意', score: '20%', plats: ['qq', 'iqiyi'] },
    { index: 7, name: '九义人', score: '10%', plats: ['qq', 'iqiyi'] },
    { index: 8, name: '莲花楼', score: '5%', plats: ['qq', 'iqiyi'] },
    { index: 9, name: '那些回不去的年少时光', score: '5%', plats: ['qq', 'iqiyi'] },
    { index: 10, name: '长相思', score: '5%', plats: ['qq', 'iqiyi'] },
  ]
}
